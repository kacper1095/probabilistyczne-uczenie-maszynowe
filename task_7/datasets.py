import pandas as pd
import numpy as np


class Dataset:
    def __init__(self, dataset, class_column):
        self.dataset = pd.DataFrame(dataset)
        self._column_name = class_column

    def __next__(self):
        return self.dataset.sample(1)[self._column_name].values

    def get_next(self):
        return self.dataset.sample(1)[self._column_name].values

    @property
    def x_labels(self):
        return pd.unique(self.dataset[self._column_name])

    @property
    def description(self):
        return self.dataset.describe()

    @property
    def classes_counts(self):
        return self.dataset[self._column_name].value_counts()


class ImmunotherapyDataset(Dataset):
    def __init__(self):
        super().__init__(pd.read_csv("data/immuno.data"), 'Result_of_Treatment')
        self.dataset["Result_of_Treatment"] = self.dataset["Result_of_Treatment"].replace({0: "positive", 1: "negative"})


class SkinDataset(Dataset):
    def __init__(self):
        super().__init__(pd.read_csv("data/skin.data"), "Label")
        self.dataset["Label"] = self.dataset["Label"].replace({1: "skin", 2: "nonskin"})


class CoinToss(Dataset):
    def __init__(self):
        rng = np.random.RandomState(0)
        data_frame = pd.DataFrame(data={'Class': rng.binomial(1, 0.5, size=(1000,))})
        super().__init__(data_frame, 'Class')

    @property
    def x_labels(self):
        return np.array(["T", "H"])


if __name__ == '__main__':
    print(CoinToss().get_next())
