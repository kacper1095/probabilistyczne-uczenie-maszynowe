import numpy as np

EPSILON = 1e-6


class DataTransform:
    def __init__(self, column_number=-1):
        self.column_number = column_number

    def fit_params(self, data, *args, **kwargs):
        raise NotImplementedError

    def transform(self, data, *args, **kwargs):
        raise NotImplementedError

    def __call__(self, data, *args, **kwargs):
        return self.transform(data)

    def get_proper_column_number(self, data):
        if self.column_number == -1:
            return np.array(list(range(data.shape[1])))
        return self.column_number


class Composer(DataTransform):
    def __init__(self, data_transforms: list):
        super().__init__(-1)
        self.data_transforms = data_transforms

    def fit_params(self, data, *args, **kwargs):
        for transform in self.data_transforms:
            transform.fit_params(data, *args, **kwargs)

    def transform(self, data, *args, **kwargs):
        for transform in self.data_transforms:
            data = transform(data, *args, **kwargs)
        return data


class ScaleToInt(DataTransform):
    def __init__(self, column_number):
        super().__init__(column_number)
        self.scaler = 1

    def fit_params(self, data, *args, **kwargs):
        data = data.copy()
        column_number = self.get_proper_column_number(data)
        data = data[:, column_number]
        while np.any(((data * self.scaler) - np.round(data * self.scaler).astype(np.int)) > EPSILON):
            self.scaler *= 10

    def transform(self, data, *args, **kwargs):
        data = data.copy()
        data = np.array(data)
        column_number = self.get_proper_column_number(data)
        data[:, column_number] *= self.scaler
        data[:, column_number] -= np.min(data[:, column_number], axis=0)
        data[:, column_number] = data[:, column_number].astype('int32')
        return data


class ToBins(DataTransform):
    def __init__(self, bins, column_number=-1):
        super().__init__(column_number)
        self.bins = bins
        self.feature_linspaces = {}

    def fit_params(self, data, *args, **kwargs):
        data = data.copy()
        column_number = self.get_proper_column_number(data)
        if isinstance(column_number, int):
            feature_column = data[:, column_number]
            linspace = np.linspace(np.min(feature_column), np.max(feature_column), self.bins)
            self.feature_linspaces[column_number] = linspace
        else:
            for i in column_number:
                feature_column = data[:, i]
                linspace = np.linspace(np.min(feature_column), np.max(feature_column), self.bins)
                self.feature_linspaces[i] = linspace

    def transform(self, data, *args, **kwargs):
        data = data.copy()
        column_number = self.get_proper_column_number(data)
        if isinstance(column_number, int):
            feature_column = data[:, column_number]
            linspace = self.feature_linspaces[column_number]
            data[:, column_number] = np.array(self._transform_single_column(feature_column, linspace))
            return data
        else:
            transformed_features = []
            for i in self.feature_linspaces.keys():
                feature_column = data[:, i]
                linspace = self.feature_linspaces[i]
                transformed_features.append(self._transform_single_column(feature_column, linspace))
            return np.array(transformed_features).T

    def _transform_single_column(self, feature_column, linspace):
        transformed_column = []
        for feature in feature_column:
            if len(linspace) == 1:
                transformed_column.append(0)
            else:
                for j in range(len(linspace) - 1):
                    if linspace[j] <= feature < linspace[j + 1]:
                        transformed_column.append(j)
                    elif j == 0 and feature < linspace[j]:
                        transformed_column.append(j)
                    elif j + 1 == len(linspace) - 1 and linspace[j + 1] <= feature:
                        transformed_column.append(j + 1)
        return transformed_column


class KMeans(DataTransform):
    def __init__(self, n_clusters, column_number=-1, n_iterations=100):
        super().__init__(column_number)
        self.n_clusters = n_clusters
        self.n_iterations = n_iterations
        self.centroids = {}

    def fit_params(self, data, *args, **kwargs):
        data = data.copy()
        column_number = self.get_proper_column_number(data)
        if isinstance(column_number, int):
            feature_column = data[:, column_number]
            linspace = np.linspace(np.min(feature_column), np.max(feature_column), self.n_clusters)
            self.centroids[column_number] = []
            for val in linspace:
                self.centroids[column_number].append(val)
            fitted_centroids = {
                column_number: self._fit_centroids(self.centroids[column_number], data[:, column_number])}
            self.centroids = fitted_centroids
        else:
            for i in column_number:
                feature_column = data[:, i]
                linspace = np.linspace(np.min(feature_column), np.max(feature_column), self.n_clusters)
                self.centroids[i] = []
                for val in linspace:
                    self.centroids[i].append(val)
            fitted_centroids = {}
            for c, f in zip(self.centroids.keys(), data.T):
                fitted_centroids[c] = self._fit_centroids(self.centroids[c], f)
            self.centroids = fitted_centroids

    def transform(self, data, *args, **kwargs):
        data = data.copy()
        column_number = self.get_proper_column_number(data)
        if isinstance(column_number, int):
            transformed_features = self._get_closest_centroid_num(self.centroids[column_number], data[:, column_number])
            data[:, column_number] = np.array(transformed_features)
            return data
        else:
            transformed_features = []
            for i in column_number:
                transformed_features.append(self._get_closest_centroid_num(self.centroids[i], data[:, i]))
            return np.array(transformed_features).T

    def _fit_centroids(self, centroids, feature_column):
        for x in range(self.n_iterations):
            classes = self._assign_step(centroids, feature_column)
            centroids = self._update_step(centroids, classes, feature_column)
        return np.array(centroids)

    def _assign_step(self, centroids, feature_column):
        return self._get_closest_centroid_num(centroids, feature_column)

    def _update_step(self, centroids, classes, feature_column):
        for i in range(len(centroids)):
            centroids[i] = np.mean(feature_column[classes == i])
        return np.array(centroids)

    def _get_closest_centroid_num(self, centroids, feature_column):
        new_column = []
        for val in feature_column:
            best_dist = float('inf')
            best_cent_num = 0
            for cent_num, centroid in enumerate(centroids):
                dist = mean_square(centroid, val)
                if dist < best_dist:
                    best_cent_num = cent_num
                    best_dist = dist
            new_column.append(best_cent_num)
        return np.array(new_column)


class EqualAreas(DataTransform):

    def __init__(self, number_of_areas, column_number=-1):
        super().__init__(column_number)
        self.number_of_areas = number_of_areas
        self.linspaces = {}

    def fit_params(self, data, *args, **kwargs):
        data = data.copy()
        column_number = self.get_proper_column_number(data)
        if isinstance(column_number, int):
            features = data[:, column_number]
            self.linspaces[column_number] = self._find_medians(features)
        else:
            for i in column_number:
                self.linspaces[i] = self._find_medians(data[:, i])

    def transform(self, data, *args, **kwargs):
        data = data.copy()
        column_number = self.get_proper_column_number(data)
        if isinstance(column_number, int):
            data[:, column_number] = self._get_closest_median_numbers(self.linspaces[column_number],
                                                                      data[:, column_number])
        else:
            for i in column_number:
                data[:, i] = self._get_closest_median_numbers(self.linspaces[i], data[:, i])
        return data

    def _find_medians(self, feature_column):
        single_group_size = len(feature_column) // self.number_of_areas
        group_sizes = []
        for i in range(self.number_of_areas - 1):
            group_sizes.append(single_group_size * (i + 1))

        groups = np.split(feature_column, group_sizes)
        medians = []
        for g in groups:
            center = (np.min(g) + np.max(g)) / 2
            medians.append(center)
        return medians

    def _get_closest_median_numbers(self, medians, feature_column):
        new_features = []
        for val in feature_column:
            best_median = -1
            best_dist = np.inf
            for i, median in enumerate(medians):
                dist = mean_square(median, val)
                if dist < best_dist:
                    best_dist = dist
                    best_median = i
            new_features.append(best_median)
        return np.array(new_features)


def mean_square(val_1, val_2):
    return 1 / 2 * (val_1 - val_2) ** 2
