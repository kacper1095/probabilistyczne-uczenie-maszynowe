import datetime
import os


def ensure_dir(path):
    if os.path.exists(path):
        return
    os.makedirs(path)


def get_time_stamp():
    return datetime.datetime.now().strftime('%HH_%MM_%dd_%mm_%Yy')
