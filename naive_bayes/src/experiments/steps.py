import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from matplotlib import cm
from collections import defaultdict
from src.common import EXPERIMENTS_DIR
from src.experiments.utils import ensure_dir, get_time_stamp
from src.data_loaders import all_datasets_with_names
from src.algorithm import NaiveBayes, GaussianNaiveBayes
from src.metrics import Accuracy, ConfusionMatrix, Recall, Precision, F1
from src.validation_utils import KFold, StratifiedKFold
from src.data_utils import EqualAreas, KMeans, ToBins


class Step:
    def __init__(self, dataset):
        self.acc = Accuracy()
        self.fscore = F1()
        self.precision = Precision()
        self.conf_matrix = ConfusionMatrix(normalize=True)
        self.recall = Recall()
        self.dataset = dataset

    def perform(self, params):
        raise NotImplementedError

    def plot_results(self, **kwargs):
        pass

    def plot_conf_matrix(self, data, out_path, classes):
        fig = plt.figure(figsize=(6, 6))
        plt.ylabel('Prawdziwe klasy')
        plt.xlabel('Predykowane klasy')
        ax = sns.heatmap(data, cmap="Blues", square=True, annot=True, xticklabels=classes,
                         yticklabels=classes, fmt='.2f', cbar=False)
        ax.set_ylabel('Prawdziwe klasy')
        ax.set_xlabel('Predykowane klasy')
        fig.savefig(out_path)

    def plot_bars(self, metric_name, plot_title, xlabel, data, out_path):
        N = len(data.keys())
        ind = np.arange(N)  # the x locations for the groups
        width = 0.35  # the width of the bars
        colors = self.get_colors_from_cmap(2)

        train_values = []
        test_values = []
        for name, (train, test) in data.items():
            train_values.append(train)
            test_values.append(test)

        fig, ax = plt.subplots(figsize=(6, 6))
        rects1 = ax.bar(ind, train_values, width, color=colors[0])

        rects2 = ax.bar(ind + width, test_values, width, color=colors[1])

        # add some text for labels, title and axes ticks
        ax.set_ylabel(metric_name)
        ax.set_xlabel(xlabel)
        ax.set_title(plot_title)
        ax.set_xticks(ind + width / 2)
        ax.set_xticklabels(list(data.keys()))

        ax.legend((rects1[0], rects2[0]), ('Treningowy', 'Walidacyjny'))
        fig.savefig(out_path)

    def plot_lines(self, metric_name, xlabel, plot_title, data, out_path):
        fig = plt.figure(figsize=(6, 6))
        colors = self.get_colors_from_cmap(2)
        plt.title(plot_title)
        plt.xlabel(xlabel)
        plt.ylabel(metric_name)
        plt.grid()
        param_values = []
        train_values = []
        test_values = []
        for param_value, train, test in data:
            param_values.append(param_value)
            train_values.append(train)
            test_values.append(test)

        plt.plot(param_values, train_values, 'o-', color=colors[0],
                 label="Treningowy")
        plt.plot(param_values, test_values, 'o--', color=colors[1],
                 label="Testowy")

        plt.legend(loc="best")
        fig.savefig(out_path)

    def get_output_path(self, folder, experiment_name, dataset_name, mode, plot_name):
        fold = os.path.join(EXPERIMENTS_DIR, folder, experiment_name, f'{dataset_name}-{mode}-{plot_name}')
        ensure_dir(os.path.dirname(fold))
        return fold

    def get_colors_from_cmap(self, nb_colors):
        all_colors = []
        for i in range(500):
            all_colors.append(cm.Blues(i))
        needed_colors = []
        for i in range(0, len(all_colors), len(all_colors) // (nb_colors + 1)):
            if i == 0:
                continue
            needed_colors.append(all_colors[i])
        return needed_colors

    def tabular_data_to_latex(self, row_names, col_names, tabular_data, output_path):
        delimiter = ' \\\\\n'
        output_string = [' & '.join(col_names) + delimiter]
        row_names = [str(name) for name in row_names]
        for row_name, row in zip(row_names, tabular_data):
            values = [str(round(val, 4)) for val in row]
            output_string.append(row_name + ' & ' + ' & '.join(values) + delimiter)
        with open(output_path, 'w') as f:
            f.writelines(output_string)


class CreateDir(Step):
    def perform(self, params):
        time_stamp = get_time_stamp()
        ensure_dir(os.path.join(EXPERIMENTS_DIR, time_stamp))

        return {
            'folder': time_stamp
        }


class BasisExperiment(Step):
    def perform(self, params):
        folder = params['folder']

        data_to_plot = {}
        classes_names = {}
        for name, dataset in all_datasets_with_names:
            dataset = dataset()

            kfold = KFold(dataset.data, dataset.target)
            data_to_plot[name] = defaultdict(list)
            classes_names[name] = dataset.classes_names
            for train_fold, test_fold in kfold:
                classifier = NaiveBayes()
                x_train, y_train = dataset.data[train_fold], dataset.target[train_fold]
                x_test, y_test = dataset.data[test_fold], dataset.target[test_fold]
                classifier.fit(x_train, y_train)

                train_pred = classifier.predict(x_train)
                test_pred = classifier.predict(x_test)

                data_to_plot[name]['y_train_pred'] += list(train_pred)
                data_to_plot[name]['y_train_true'] += list(y_train)

                data_to_plot[name]['y_test_pred'] += list(test_pred)
                data_to_plot[name]['y_test_true'] += list(y_test)

        self.plot_results(data_to_plot=data_to_plot, folder=folder, classes_names=classes_names, params=all_datasets_with_names)

    def plot_results(self, **kwargs):
        data = kwargs['data_to_plot']
        classes_names = kwargs['classes_names']
        folder = kwargs['folder']
        params = kwargs['params']

        dataset_metric_name_results = defaultdict(dict)

        for dataset_name, results in data.items():
            acc_train = self.acc(results['y_train_pred'], results['y_train_true'])
            f1_train = self.fscore(results['y_train_pred'], results['y_train_true'])
            confusion_train = self.conf_matrix(results['y_train_pred'], results['y_train_true'])

            acc_test = self.acc(results['y_test_pred'], results['y_test_true'])
            f1_test = self.fscore(results['y_test_pred'], results['y_test_true'])
            confusion_test = self.conf_matrix(results['y_test_pred'], results['y_test_true'])

            self.plot_conf_matrix(confusion_train,
                                  self.get_output_path(folder, self.__class__.__name__, dataset_name, 'train',
                                                       'conf-matrix.png'),
                                  classes_names[dataset_name])

            self.plot_conf_matrix(confusion_test,
                                  self.get_output_path(folder, self.__class__.__name__, dataset_name, 'test',
                                                       'conf-matrix.png'),
                                  classes_names[dataset_name])

            dataset_metric_name_results['Accuracy'][dataset_name] = (acc_train, acc_test)
            dataset_metric_name_results['F1 Score'][dataset_name] = (f1_train, f1_test)

        for metric_name, data in dataset_metric_name_results.items():
            self.plot_bars(metric_name, metric_name + ' w zależności od zbioru danych', 'Zbiór danych', data,
                           self.get_output_path(folder, self.__class__.__name__, 'all', 'all',
                                                f'{metric_name}-bars.png'))
        tabular_data = []
        for param_name, _ in params:
            tabular_data.append([])
            for key in dataset_metric_name_results.keys():
                tabular_data[-1] += list(dataset_metric_name_results[key][param_name])
        self.tabular_data_to_latex([name.capitalize() for name, _ in all_datasets_with_names],
                                   ['Zbiór danych', 'Accuracy (treningowy)', 'Accuracy (testowy)', 'F1 Score (treningowy)', 'F1 Score (testowy)'],
                                   tabular_data,
                                   self.get_output_path(folder, self.__class__.__name__, 'all', 'all', 'output.txt'))


class CrossValidationExperiment(Step):
    def perform(self, params):
        folder = params['folder']
        params = [('Zwykly', KFold), ('Stratyfikowany', StratifiedKFold)]

        data_to_plot = {}
        classes_names = {}
        for name, folding in params:
            dataset = self.dataset()

            kfold = folding(dataset.data, dataset.target, folds=10)
            data_to_plot[name] = defaultdict(list)
            classes_names[name] = dataset.classes_names
            for train_fold, test_fold in kfold:
                transformer = KMeans(5, -1)
                classifier = NaiveBayes(transformer, smooth_data=True)
                x_train, y_train = dataset.data[train_fold], dataset.target[train_fold]
                x_test, y_test = dataset.data[test_fold], dataset.target[test_fold]
                transformer.fit_params(x_train, y_train)
                classifier.fit(x_train, y_train)

                train_pred = classifier.predict(x_train)
                test_pred = classifier.predict(x_test)

                data_to_plot[name]['y_train_pred'] += list(train_pred)
                data_to_plot[name]['y_train_true'] += list(y_train)

                data_to_plot[name]['y_test_pred'] += list(test_pred)
                data_to_plot[name]['y_test_true'] += list(y_test)

        self.plot_results(data_to_plot=data_to_plot, folder=folder, classes_names=classes_names, params=params)

    def plot_results(self, **kwargs):
        data = kwargs['data_to_plot']
        classes_names = kwargs['classes_names']
        folder = kwargs['folder']
        params = kwargs['params']

        dataset_metric_name_results = defaultdict(dict)

        for dataset_name, results in data.items():
            acc_train = self.acc(results['y_train_pred'], results['y_train_true'])
            f1_train = self.fscore(results['y_train_pred'], results['y_train_true'])
            confusion_train = self.conf_matrix(results['y_train_pred'], results['y_train_true'])

            acc_test = self.acc(results['y_test_pred'], results['y_test_true'])
            f1_test = self.fscore(results['y_test_pred'], results['y_test_true'])
            confusion_test = self.conf_matrix(results['y_test_pred'], results['y_test_true'])

            self.plot_conf_matrix(confusion_train,
                                  self.get_output_path(folder, self.__class__.__name__, dataset_name, 'train',
                                                       'conf-matrix.png'),
                                  classes_names[dataset_name])

            self.plot_conf_matrix(confusion_test,
                                  self.get_output_path(folder, self.__class__.__name__, dataset_name, 'test',
                                                       'conf-matrix.png'),
                                  classes_names[dataset_name])

            dataset_metric_name_results['Accuracy'][dataset_name] = (acc_train, acc_test)
            dataset_metric_name_results['F1 Score'][dataset_name] = (f1_train, f1_test)

        for metric_name, data in dataset_metric_name_results.items():
            self.plot_bars(metric_name, metric_name + ' w zależności od metody podziału danych', 'Podział danych', data,
                           self.get_output_path(folder, self.__class__.__name__, 'all', 'all',
                                                f'{metric_name}-bars.png'))
        tabular_data = []
        for param_name, _ in params:
            tabular_data.append([])
            for key in dataset_metric_name_results.keys():
                tabular_data[-1] += list(dataset_metric_name_results[key][param_name])
        self.tabular_data_to_latex(['Zwykły', 'Stratyfikowany'],
                                   ['Podział danych', 'Accuracy (treningowy)', 'Accuracy (testowy)', 'F1 Score (treningowy)', 'F1 Score (testowy)'],
                                   tabular_data,
                                   self.get_output_path(folder, self.__class__.__name__, 'all', 'all', 'output.txt'))


class GaussianPrioriExperiment(Step):
    def perform(self, params):
        folder = params['folder']
        params = [('Bez zalozenia', False), ('Z zalozeniem o rozkladzie normalnym', True)]

        data_to_plot = {}
        classes_names = {}
        for name, apriori in params:
            dataset = self.dataset()

            kfold = KFold(dataset.data, dataset.target, folds=10)
            data_to_plot[name] = defaultdict(list)
            classes_names[name] = dataset.classes_names
            for train_fold, test_fold in kfold:
                x_train, y_train = dataset.data[train_fold], dataset.target[train_fold]
                x_test, y_test = dataset.data[test_fold], dataset.target[test_fold]

                if apriori:
                    classifier = GaussianNaiveBayes()
                else:
                    transformer = KMeans(6, -1)
                    classifier = NaiveBayes(transformer, smooth_data=True)
                    transformer.fit_params(x_train, y_train)
                classifier.fit(x_train, y_train)

                train_pred = classifier.predict(x_train)
                test_pred = classifier.predict(x_test)

                data_to_plot[name]['y_train_pred'] += list(train_pred)
                data_to_plot[name]['y_train_true'] += list(y_train)

                data_to_plot[name]['y_test_pred'] += list(test_pred)
                data_to_plot[name]['y_test_true'] += list(y_test)

        self.plot_results(data_to_plot=data_to_plot, folder=folder, classes_names=classes_names, params=params)

    def plot_results(self, **kwargs):
        data = kwargs['data_to_plot']
        classes_names = kwargs['classes_names']
        folder = kwargs['folder']
        params = kwargs['params']

        dataset_metric_name_results = defaultdict(dict)

        for dataset_name, results in data.items():
            acc_train = self.acc(results['y_train_pred'], results['y_train_true'])
            f1_train = self.fscore(results['y_train_pred'], results['y_train_true'])
            confusion_train = self.conf_matrix(results['y_train_pred'], results['y_train_true'])

            acc_test = self.acc(results['y_test_pred'], results['y_test_true'])
            f1_test = self.fscore(results['y_test_pred'], results['y_test_true'])
            confusion_test = self.conf_matrix(results['y_test_pred'], results['y_test_true'])

            self.plot_conf_matrix(confusion_train,
                                  self.get_output_path(folder, self.__class__.__name__, dataset_name, 'train',
                                                       'conf-matrix.png'),
                                  classes_names[dataset_name])

            self.plot_conf_matrix(confusion_test,
                                  self.get_output_path(folder, self.__class__.__name__, dataset_name, 'test',
                                                       'conf-matrix.png'),
                                  classes_names[dataset_name])

            dataset_metric_name_results['Accuracy'][dataset_name] = (acc_train, acc_test)
            dataset_metric_name_results['F1 Score'][dataset_name] = (f1_train, f1_test)

        for metric_name, data in dataset_metric_name_results.items():
            self.plot_bars(metric_name, metric_name + ' w zależności od założenia o danych', 'Założenie o rozkładzie normalnym', data,
                           self.get_output_path(folder, self.__class__.__name__, 'all', 'all',
                                                f'{metric_name}-bars.png'))

        tabular_data = []
        for param_name, _ in params:
            tabular_data.append([])
            for key in dataset_metric_name_results.keys():
                tabular_data[-1] += list(dataset_metric_name_results[key][param_name])
        self.tabular_data_to_latex(['Bez założenia', 'Z założeniem o rozkładzie normalnym'],
                                   ['Metoda', 'Accuracy (treningowy)', 'Accuracy (testowy)', 'F1 Score (treningowy)', 'F1 Score (testowy)'],
                                   tabular_data,
                                   self.get_output_path(folder, self.__class__.__name__, 'all', 'all', 'output.txt'))


class DiscretizationMethodsExperiment(Step):
    def __init__(self, dataset, test_param_range):
        super().__init__(dataset)
        self.test_param_range = test_param_range

    def perform(self, params):
        folder = params['folder']
        methods = [('Algorytm K-srednich', KMeans), ('Histogram', ToBins), ('Rowne Obszary', EqualAreas)]
        param_range = self.test_param_range

        data_to_plot = {}
        metric_param_val_result = defaultdict(dict)
        classes_names = {}
        for name, method in methods:
            for val in param_range:
                dataset = self.dataset()
                metric_param_val_result[name][val] = defaultdict(list)

                kfold = KFold(dataset.data, dataset.target)
                data_to_plot[name] = defaultdict(list)
                classes_names[name] = dataset.classes_names
                for train_fold, test_fold in kfold:
                    transformer = method(val, -1)
                    classifier = NaiveBayes(transformer)
                    x_train, y_train = dataset.data[train_fold], dataset.target[train_fold]
                    x_test, y_test = dataset.data[test_fold], dataset.target[test_fold]

                    transformer.fit_params(x_train, y_train)
                    classifier.fit(x_train, y_train)

                    train_pred = classifier.predict(x_train)
                    test_pred = classifier.predict(x_test)

                    data_to_plot[name]['y_train_pred'] += list(train_pred)
                    data_to_plot[name]['y_train_true'] += list(y_train)

                    data_to_plot[name]['y_test_pred'] += list(test_pred)
                    data_to_plot[name]['y_test_true'] += list(y_test)

                    metric_param_val_result[name][val]['y_train_pred'] += list(train_pred)
                    metric_param_val_result[name][val]['y_train_true'] += list(y_train)

                    metric_param_val_result[name][val]['y_test_pred'] += list(test_pred)
                    metric_param_val_result[name][val]['y_test_true'] += list(y_test)

        self.plot_results(data_to_plot=data_to_plot, metric_param_val_result=metric_param_val_result,
                          folder=folder, classes_names=classes_names, params=methods)

    def plot_results(self, **kwargs):
        data = kwargs['data_to_plot']
        classes_names = kwargs['classes_names']
        folder = kwargs['folder']
        metric_param_val_result = kwargs['metric_param_val_result']
        params = kwargs['params']

        method_metric_name_results = defaultdict(dict)
        plot_data_metric_param_val_result = defaultdict(list)

        for method_name, results in data.items():
            acc_train = self.acc(results['y_train_pred'], results['y_train_true'])
            f1_train = self.fscore(results['y_train_pred'], results['y_train_true'])
            confusion_train = self.conf_matrix(results['y_train_pred'], results['y_train_true'])

            acc_test = self.acc(results['y_test_pred'], results['y_test_true'])
            f1_test = self.fscore(results['y_test_pred'], results['y_test_true'])
            confusion_test = self.conf_matrix(results['y_test_pred'], results['y_test_true'])

            self.plot_conf_matrix(confusion_train,
                                  self.get_output_path(folder, self.__class__.__name__, method_name, 'train',
                                                       'conf-matrix.png'),
                                  classes_names[method_name])

            self.plot_conf_matrix(confusion_test,
                                  self.get_output_path(folder, self.__class__.__name__, method_name, 'test',
                                                       'conf-matrix.png'),
                                  classes_names[method_name])

            method_metric_name_results['Accuracy'][method_name] = (acc_train, acc_test)
            method_metric_name_results['F1 Score'][method_name] = (f1_train, f1_test)

        for metric_name, data in method_metric_name_results.items():
            self.plot_bars(metric_name, metric_name + ' w zależności metody dyskretyzacji', 'Metoda dyskretyzacji', data,
                           self.get_output_path(folder, self.__class__.__name__, 'all', 'all',
                                                f'{metric_name}-bars.png'))

        for method_name, param_stats in metric_param_val_result.items():
            for param_value, results in param_stats.items():
                f1_train = self.fscore(results['y_train_pred'], results['y_train_true'])
                f1_test = self.fscore(results['y_test_pred'], results['y_test_true'])

                plot_data_metric_param_val_result[method_name].append((param_value, f1_train, f1_test))

        for method_name, data in plot_data_metric_param_val_result.items():
            self.plot_lines('F1 Score', 'Wielkość podziału', method_name.capitalize(), data,
                            self.get_output_path(folder, self.__class__.__name__, method_name, 'fscore', f'curve.png'))

        tabular_data = []
        for param_name, _ in params:
            tabular_data.append([])
            for key in method_metric_name_results.keys():
                tabular_data[-1] += list(method_metric_name_results[key][param_name])
        self.tabular_data_to_latex([name.capitalize() for name, _ in params],
                                   ['Metoda', 'Accuracy (treningowy)', 'Accuracy (testowy)', 'F1 Score (treningowy)', 'F1 Score (testowy)'],
                                   tabular_data,
                                   self.get_output_path(folder, self.__class__.__name__, 'all', 'all', 'output.txt'))

        for method, values in plot_data_metric_param_val_result.items():
            param_values = []
            for p_value, f_train, f_test in values:
                param_values.append(p_value)
            self.tabular_data_to_latex(param_values,
                                       ['Wielkość podziału', 'F1 Score (treningowy)', 'F1 Score (testowy)'],
                                       values,
                                       self.get_output_path(folder, self.__class__.__name__,
                                                            method, 'all', 'output.txt'))


class DataSmoothingExperiment(Step):
    def perform(self, params):
        folder = params['folder']
        params = [('Bez wygladzania', False), ('Wygladzanie', True)]

        data_to_plot = {}
        classes_names = {}
        for name, smoothing in params:
            dataset = self.dataset()

            kfold = KFold(dataset.data, dataset.target, folds=10)
            data_to_plot[name] = defaultdict(list)
            classes_names[name] = dataset.classes_names
            for train_fold, test_fold in kfold:
                transformer = KMeans(3, -1)
                classifier = NaiveBayes(transformer, smooth_data=smoothing)
                x_train, y_train = dataset.data[train_fold], dataset.target[train_fold]
                x_test, y_test = dataset.data[test_fold], dataset.target[test_fold]
                transformer.fit_params(x_train, y_train)
                classifier.fit(x_train, y_train)

                train_pred = classifier.predict(x_train)
                test_pred = classifier.predict(x_test)

                data_to_plot[name]['y_train_pred'] += list(train_pred)
                data_to_plot[name]['y_train_true'] += list(y_train)

                data_to_plot[name]['y_test_pred'] += list(test_pred)
                data_to_plot[name]['y_test_true'] += list(y_test)

        self.plot_results(data_to_plot=data_to_plot, folder=folder, classes_names=classes_names, params=params)

    def plot_results(self, **kwargs):
        data = kwargs['data_to_plot']
        classes_names = kwargs['classes_names']
        folder = kwargs['folder']
        params = kwargs['params']

        dataset_metric_name_results = defaultdict(dict)

        for dataset_name, results in data.items():
            acc_train = self.acc(results['y_train_pred'], results['y_train_true'])
            f1_train = self.fscore(results['y_train_pred'], results['y_train_true'])
            confusion_train = self.conf_matrix(results['y_train_pred'], results['y_train_true'])

            acc_test = self.acc(results['y_test_pred'], results['y_test_true'])
            f1_test = self.fscore(results['y_test_pred'], results['y_test_true'])
            confusion_test = self.conf_matrix(results['y_test_pred'], results['y_test_true'])

            self.plot_conf_matrix(confusion_train,
                                  self.get_output_path(folder, self.__class__.__name__, dataset_name, 'train',
                                                       'conf-matrix.png'),
                                  classes_names[dataset_name])

            self.plot_conf_matrix(confusion_test,
                                  self.get_output_path(folder, self.__class__.__name__, dataset_name, 'test',
                                                       'conf-matrix.png'),
                                  classes_names[dataset_name])

            dataset_metric_name_results['Accuracy'][dataset_name] = (acc_train, acc_test)
            dataset_metric_name_results['F1 Score'][dataset_name] = (f1_train, f1_test)

        for metric_name, data in dataset_metric_name_results.items():
            self.plot_bars(metric_name, metric_name + ' w zależności od wygładzania danych', 'Wygładzanie danych', data,
                           self.get_output_path(folder, self.__class__.__name__, 'all', 'all',
                                                f'{metric_name}-bars.png'))

        tabular_data = []
        for param_name, _ in params:
            tabular_data.append([])
            for key in dataset_metric_name_results.keys():
                tabular_data[-1] += list(dataset_metric_name_results[key][param_name])
        self.tabular_data_to_latex(['Bez wygładzania', 'Z wygładzaniem'],
                                   ['Metoda', 'Accuracy (treningowy)', 'Accuracy (testowy)', 'F1 Score (treningowy)', 'F1 Score (testowy)'],
                                   tabular_data,
                                   self.get_output_path(folder, self.__class__.__name__, 'all', 'all', 'output.txt'))


class FinalExperiment(Step):
    def perform(self, params):
        folder = params['folder']

        data_to_plot = {}
        classes_names = {}
        for name, dataset in all_datasets_with_names:
            dataset = dataset()

            kfold = StratifiedKFold(dataset.data, dataset.target)
            data_to_plot[name] = defaultdict(list)
            classes_names[name] = dataset.classes_names
            for train_fold, test_fold in kfold:
                x_train, y_train = dataset.data[train_fold], dataset.target[train_fold]
                x_test, y_test = dataset.data[test_fold], dataset.target[test_fold]
                transformer = KMeans(6, -1)
                transformer.fit_params(x_train, y_train)
                classifier = NaiveBayes(transformer, smooth_data=True)
                classifier.fit(x_train, y_train)

                train_pred = classifier.predict(x_train)
                test_pred = classifier.predict(x_test)

                data_to_plot[name]['y_train_pred'] += list(train_pred)
                data_to_plot[name]['y_train_true'] += list(y_train)

                data_to_plot[name]['y_test_pred'] += list(test_pred)
                data_to_plot[name]['y_test_true'] += list(y_test)

        self.plot_results(data_to_plot=data_to_plot, folder=folder, classes_names=classes_names, params=all_datasets_with_names)

    def plot_results(self, **kwargs):
        data = kwargs['data_to_plot']
        classes_names = kwargs['classes_names']
        folder = kwargs['folder']
        params = kwargs['params']

        dataset_metric_name_results = defaultdict(dict)

        for dataset_name, results in data.items():
            acc_train = self.acc(results['y_train_pred'], results['y_train_true'])
            f1_train = self.fscore(results['y_train_pred'], results['y_train_true'])
            confusion_train = self.conf_matrix(results['y_train_pred'], results['y_train_true'])

            acc_test = self.acc(results['y_test_pred'], results['y_test_true'])
            f1_test = self.fscore(results['y_test_pred'], results['y_test_true'])
            confusion_test = self.conf_matrix(results['y_test_pred'], results['y_test_true'])

            self.plot_conf_matrix(confusion_train,
                                  self.get_output_path(folder, self.__class__.__name__, dataset_name, 'train',
                                                       'conf-matrix.png'),
                                  classes_names[dataset_name])

            self.plot_conf_matrix(confusion_test,
                                  self.get_output_path(folder, self.__class__.__name__, dataset_name, 'test',
                                                       'conf-matrix.png'),
                                  classes_names[dataset_name])

            dataset_metric_name_results['Accuracy'][dataset_name] = (acc_train, acc_test)
            dataset_metric_name_results['F1 Score'][dataset_name] = (f1_train, f1_test)

        for metric_name, data in dataset_metric_name_results.items():
            self.plot_bars(metric_name, metric_name + ' w zależności od zbioru danych', 'Zbiór danych', data,
                           self.get_output_path(folder, self.__class__.__name__, 'all', 'all',
                                                f'{metric_name}-bars.png'))
        tabular_data = []
        for param_name, _ in params:
            tabular_data.append([])
            for key in dataset_metric_name_results.keys():
                tabular_data[-1] += list(dataset_metric_name_results[key][param_name])
        self.tabular_data_to_latex([name.capitalize() for name, _ in all_datasets_with_names],
                                   ['Zbiór danych', 'Accuracy (treningowy)', 'Accuracy (testowy)', 'F1 Score (treningowy)', 'F1 Score (testowy)'],
                                   tabular_data,
                                   self.get_output_path(folder, self.__class__.__name__, 'all', 'all', 'output.txt'))

