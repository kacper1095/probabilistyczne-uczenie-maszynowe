from collections import Counter, defaultdict, OrderedDict
from typing import Iterator, TypeVar, Dict, Callable, Any
from src.common import EPSILON

import numpy as np
import pandas as pd

C = TypeVar('C', bound='Classifier')


class Classifier:
    def fit(self, x: np.ndarray, y: np.ndarray) -> C:
        raise NotImplementedError

    def predict(self, x: np.ndarray) -> np.ndarray:
        raise NotImplementedError

    def score(self, x: np.ndarray, y: np.ndarray, metrics: Iterator[Callable] or Callable) -> Dict[str, float]:
        result = {}
        prediction = self.predict(x)
        if isinstance(metrics, list):
            for metric in metrics:
                result[metric.__name__] = metric(prediction, y)
        else:
            return {metrics.__name__: metrics(prediction, y)}
        return result


class NaiveBayes(Classifier):
    def __init__(self, digitization_method=None, smooth_data=False):
        """
        Creates NaiveBayes classifier with digitizing continuous values.
        :param discretization_bins: Bins of discretization of continuous values in the feature vector. If ``int``, all
            values are passed to the same number of bins. Else, it should be the same length as feature vector and each
            number of bins is passed for each feature.

            If `digitization_method` is not `None`, then this parameter is ignored.
        :type discretization_bins: list or tuple or int.
        :param digitization_method: Func with custom digitzation method. (nd.array (n_samples, n_features)) -> (nd.array (n_samples, n_features))
        :type digitization_method: callable
        """
        self.digitization_method = digitization_method

        self.params = []
        self.class_probas = defaultdict(float)
        self.samples_count = 0
        self.smooth_data = smooth_data

    def fit(self, x, y):
        """
        Fits parameters of :class:`NaiveBayes` to the data.
        :param x: np.ndarray in shape (n_samples, n_features).
        :param y: np.ndarray in shape (n_samples,) where each row represents number of class.
        :return: None
        """
        individual_classes_count = Counter(y)
        individual_classes_count_proba = {cls: [count / len(y), count] for cls, count in
                                          individual_classes_count.items()}
        self.class_probas = individual_classes_count_proba

        if self.digitization_method is not None:
            x = self.digitization_method(x)
        unique_classes = np.unique(y)

        for i in range(x.shape[1]):
            unique_values = np.unique(x[:, i])
            new_table = defaultdict(dict)
            for y_cord, val in enumerate(unique_values):
                for x_cord, cls in enumerate(unique_classes):
                    count = ((x[:, i] == val) & (y == cls)).sum()
                    new_table[y_cord][cls] = count + int(self.smooth_data)

            self.params.append(new_table)

        return self

    def get_params(self, normalized=False):
        frames = {}
        for i in range(len(self.params)):
            df_dict = OrderedDict()
            attribute_values = []
            for attribute_value, classes in self.params[i].items():
                for cls, cls_count in sorted(classes.items(), key=lambda x: int(x[0])):
                    a_key = f'Y={cls}'
                    if a_key not in df_dict:
                        df_dict[a_key] = []
                    df_dict[a_key].append(cls_count)
                attribute_values.append(f'X_{i}={attribute_value}')
            df = pd.DataFrame(df_dict, index=attribute_values)
            if normalized:
                a_sum = df.sum(axis=1)
                df = df.div(a_sum, axis=0)
            frames[i] = df
        return frames

    def predict(self, x: np.ndarray):
        if self.digitization_method is not None:
            x = self.digitization_method(x)
        result = []
        for sample in x:
            sample_probas = []
            sample_cls = []
            for cls, [proba, cls_count] in self.class_probas.items():
                sample_probas.append(
                    proba * np.prod([self._get_per_cls_proba(i, sample, cls, cls_count) for i in range(x.shape[1])]))
                sample_cls.append(cls)
            index = np.argmax(sample_probas)
            result.append(sample_cls[index])
        return np.array(result)

    def _get_per_cls_proba(self, attr_num, predicted_sample, cls_num, cls_count):
        attr_params = self.params[attr_num]
        cls_of_attr_params = attr_params.get(predicted_sample[attr_num], {cls_num: int(self.smooth_data)})
        if cls_num not in cls_of_attr_params.keys():
            param = self.smooth_data
        else:
            param = cls_of_attr_params[cls_num]
        proba = param / cls_count
        return proba


class GaussianNaiveBayes(Classifier):
    def __init__(self):
        self.class_proba = defaultdict(float)
        self.params = defaultdict(dict)
        self.epsilon = -1e-10

    def fit(self, x, y):
        individual_classes_count = Counter(y)
        individual_classes_count_proba = {cls: count / len(y) for cls, count in individual_classes_count.items()}
        self.class_proba = individual_classes_count_proba

        unique_classes = np.unique(y)
        for unique_class in unique_classes:
            self.params[unique_class]['mi'] = np.mean(x[y == unique_class], axis=0)
            self.params[unique_class]['sigma'] = np.std(x[y == unique_class], axis=0)
        return self

    def predict(self, x):
        probas = []
        classes = []
        for cls, proba in self.class_proba.items():
            p_xi_y = 1 / (np.sqrt(2 * np.pi * (self.params[cls]['sigma']) ** 2) + EPSILON) * np.exp(
                - (x - self.params[cls]['mi']) ** 2 / (2 * (self.params[cls]['sigma']) ** 2 + EPSILON))
            p = proba * np.prod(p_xi_y, axis=1)
            probas.append(p)
            classes.append(cls)

        index = np.argmax(probas, axis=0)
        return np.array(classes)[index]

    def get_params(self, normalized=False):
        classes = list(self.params.keys())
        frames = {}
        number_of_attributes = len(self.params[classes[0]]['mi'])
        for i in range(number_of_attributes):
            df_dict = defaultdict(list)
            classes_columns = []
            for cls in classes:
                df_dict[f'mi_{i}'].append(self.params[cls]['mi'][i])
                df_dict[f'sigma_{i}'].append(self.params[cls]['sigma'][i])
                classes_columns.append(f'Y={cls}')
            df = pd.DataFrame(df_dict, index=classes_columns)
            df = df.transpose()
            if normalized:
                a_sum = df.sum(axis=1)
                df = df.div(a_sum, axis=0)
            frames[i] = df
        return frames
