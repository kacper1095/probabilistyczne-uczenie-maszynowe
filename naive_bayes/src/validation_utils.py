import numpy as np
import warnings
import itertools

from collections import Counter


class TrainTestSplit:
    def __init__(self, train_percentage, shuffle=True, stratified=True):
        assert 0 < train_percentage < 1
        self.train_percentage = train_percentage
        self.shuffle = shuffle
        self.stratified = stratified

    def split(self, x, y):
        if self.shuffle:
            perm = np.random.permutation(len(x))
            y = y[perm]
            x = x[perm]

        if self.stratified:
            unique_classes = np.unique(y)
            indices = list(range(len(y)))
            indices_classes = list(zip(indices, y))
            grouping = itertools.groupby(indices_classes, key=lambda x: x[1])
            train_x = []
            train_y = []
            valid_x = []
            valid_y = []
            for key, group in grouping:
                group = list(group)
                group_size = int(round(len(group) * self.train_percentage))
                train_group = list(map(lambda x: x[0], group[:group_size]))
                valid_group = list(map(lambda x: x[0], group[group_size:]))

                if len(train_group) > 0:
                    train_x += list(x[train_group])
                    train_y += list(y[train_group])

                if len(valid_group) > 0:
                    valid_x += list(x[valid_group])
                    valid_y += list(y[valid_group])
            train_x = np.array(train_x)
            train_y = np.array(train_y)
            valid_x = np.array(valid_x)
            valid_y = np.array(valid_y)

        else:
            train_size = int(self.train_percentage * len(x))
            train_x, train_y = x[:train_size], y[:train_size]
            valid_x, valid_y = x[train_size:], y[train_size:]

        return (train_x, train_y), (valid_x, valid_y)


class OverSampler:
    def resample(self, x, y):
        counter = Counter(y)
        max_count = np.max(list(counter.values()))
        unique_classes = np.unique(y)

        new_x = []
        new_y = []

        for cls in unique_classes:
            cls_count = counter[cls]
            indices = np.where(np.array(y) == cls)[0]
            while cls_count < max_count:
                index = np.random.choice(indices)
                new_x.append(x[index])
                new_y.append(y[index])
                cls_count += 1
        return np.concatenate((x, new_x)), np.concatenate((y, new_y))


class KFold:
    def __init__(self, x, y, folds=10, shuffle=False):
        self.x = x
        self.y = y
        self.folds = folds
        self.shuffle = shuffle
        self.fold_size = len(x) // folds

        self.initial_range = np.array(list(range(len(x))))
        if shuffle:
            perm = np.random.permutation(len(x))
            self.initial_range = self.initial_range[perm]

        self.indices = self.generate_indices()

    def __iter__(self):
        val = next(self)
        return val

    def __next__(self):
        for i in range(self.folds):
            yield self.indices[i]

    def generate_indices(self):
        generated_indices = []
        folds = []
        for i in range(self.folds):
            folds.append(self.initial_range[i * self.fold_size: (i + 1) * self.fold_size])
        for i in range(self.folds):
            if i == 0:
                generated_indices.append((np.array(np.concatenate(folds[i + 1:])), np.array(folds[i])))
            elif i == self.folds - 1:
                generated_indices.append((np.array(np.concatenate(folds[:i])), np.array(folds[i])))
            else:
                generated_indices.append(
                    (np.concatenate((np.concatenate(folds[:i]), np.concatenate(folds[i + 1:]))), np.array(folds[i])))
        return generated_indices


class StratifiedKFold(KFold):
    def __init__(self, x, y, folds=10, shuffle=False):
        super().__init__(x, y, folds, shuffle)

    def generate_indices(self):
        unique_classes = np.unique(self.y)
        stratas = []
        for unique_class in unique_classes:
            stratas.append(self.initial_range[self.y == unique_class])

        stratas_contribution = {cls: (round(len(strata) / self.folds), strata) for cls, strata in
                                zip(unique_classes, stratas)}
        generated_indices = []
        folds = []
        for i in range(self.folds):
            stratas_single_fold = []
            for cls, (strata_contrib, strata) in stratas_contribution.items():
                if strata_contrib == 0:
                    warnings.warn('Not stratifiable dataset. Class size vs fold size: {} vs {}'.format(len(strata),
                                                                                                       self.fold_size))
                    if i < len(strata):
                        stratas_single_fold.append(strata[i:i + 1])
                else:
                    stratas_single_fold.append(strata[i * strata_contrib: (i + 1) * strata_contrib])
            folds.append(np.concatenate(stratas_single_fold))
        for i in range(self.folds):
            if i == 0:
                generated_indices.append((np.array(np.concatenate(folds[i + 1:])), np.array(folds[i])))
            elif i == self.folds - 1:
                generated_indices.append((np.array(np.concatenate(folds[:i])), np.array(folds[i])))
            else:
                generated_indices.append(
                    (np.concatenate((np.concatenate(folds[:i]), np.concatenate(folds[i + 1:]))), np.array(folds[i])))
        return generated_indices
