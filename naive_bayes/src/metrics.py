import numpy as np

from src.common import EPSILON


class Metric:
    def call(self, y_pred, y_true):
        raise NotImplementedError

    def __call__(self, y_pred, y_true):
        return self.call(y_pred, y_true)

    @property
    def __name__(self):
        return self.__class__.__name__


class Accuracy(Metric):
    def call(self, y_pred, y_true):
        if len(y_pred) == 0:
            return 0
        return (np.array(y_true) == np.array(y_pred)).sum() / (len(y_pred) + EPSILON)


class Precision(Metric):
    def __init__(self, classes=None):
        self.classes = classes

    def call(self, y_pred, y_true):
        self.classes = np.unique(y_true)
        metric_count = []
        for c in self.classes:
            casted_y_true = y_true == c
            casted_y_pred = y_pred == c
            false_positives = (casted_y_pred & (1 - casted_y_true)).sum()
            true_positives = (casted_y_pred & casted_y_true).sum()
            metric_count.append((true_positives / (true_positives + false_positives + EPSILON), casted_y_true.sum()))
        weights_sum = 0
        average = 0
        for m, c in metric_count:
            average += m * c
            weights_sum += c
        return average / (weights_sum + EPSILON)


class Recall(Metric):
    def __init__(self, classes=None):
        self.classes = classes

    def call(self, y_pred, y_true):
        self.classes = np.unique(y_true)
        metric_count = []
        for c in self.classes:
            casted_y_true = y_true == c
            casted_y_pred = y_pred == c
            false_negatives = ((1 - casted_y_pred) & casted_y_true).sum()
            true_positives = (casted_y_pred & casted_y_true).sum()
            metric_count.append((true_positives / (true_positives + false_negatives + EPSILON), casted_y_true.sum()))
        weights_sum = 0
        average = 0
        for m, c in metric_count:
            average += m * c
            weights_sum += c
        return average / (weights_sum + EPSILON)


class F1(Metric):
    def __init__(self, classes=None):
        self.classes = classes
        self.precision = Precision(classes)
        self.recall = Recall(classes)

    def call(self, y_pred, y_true):
        self.classes = np.unique(y_true)
        precision = self.precision(y_pred, y_true)
        recall = self.recall(y_pred, y_true)

        return 2 * (precision * recall) / (precision + recall + EPSILON)


class ConfusionMatrix(Metric):
    def __init__(self, classes=None, normalize=False):
        self.classes = classes
        self.normalize = normalize

    def call(self, y_pred, y_true):
        self.classes = np.unique(y_true)
        min_class = np.min(self.classes)
        result_matrix = np.zeros((np.max(self.classes) + 1 - min_class,
                                  np.max(self.classes) + 1 - min_class))

        # rows are y_pred, cols y_true
        for c_1 in self.classes:
            for c_2 in self.classes:
                casted_y_true = y_true == c_1
                casted_y_pred = y_pred == c_2
                result_matrix[c_1 - min_class, c_2 - min_class] = (casted_y_true & casted_y_pred).sum()
        if self.normalize:
            result_matrix /= (np.sum(result_matrix, axis=1)[:, np.newaxis] + EPSILON)
        return result_matrix
