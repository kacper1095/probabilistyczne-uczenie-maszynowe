from sklearn import datasets
from sklearn.naive_bayes import GaussianNB
from src.validation_utils import KFold, StratifiedKFold, OverSampler, TrainTestSplit
from src.data_loaders import *

splitter = TrainTestSplit(0.7, shuffle=False)
sampler = OverSampler()
iris = AbaloneLoader()
# iris = datasets.load_diabetes()
gnb = GaussianNB()
y_pred2 = gnb.fit(iris.data, iris.target).predict(iris.data)
print(
    "Number of mislabeled points out of a total %d points : %d" % (iris.data.shape[0], (iris.target != y_pred2).sum()))

from src.algorithm import NaiveBayes, GaussianNaiveBayes
from src.metrics import *
from src.data_utils import *

# binner = KMeans(6, 1)
# k = KMeans(6, 2)
# k22 = KMeans(6, 3)
transformer = KMeans(6, list(range(1, 8)))
# transformer = Composer([
#     binner,
#     transformer,
#     k,
#     k22   
# ])
# transformer = ToBins(1, -1)
# transformer= EqualAreas(5, -1)

transformer.fit_params(iris.data)
gnb = NaiveBayes(digitization_method=transformer)
# gnb = NaiveBayes()

(train_x, train_y), (valid_x, valid_y) = splitter.split(iris.data, iris.target)

# train_x, train_y = sampler.resample(train_x, train_y)
gnb.fit(train_x, train_y)
y_pred = gnb.predict(valid_x)
target = valid_y
print(f"Acc: {Accuracy()(y_pred, target)}")
print(f"Precision: {Precision()(y_pred, target)}")
print(f"Recall: {Recall()(y_pred, target)}")
print(f"F1: {F1()(y_pred, target)}")
# print(f"Confusion matrix: {ConfusionMatrix(normalize=True)(y_pred, target.astype(np.int))}")
print("Number of mislabeled points out of a total %d points : %d" % (
target.shape[0], (target != y_pred).sum()))
print(gnb.get_params(normalized=True))

# folder = KFold(iris.data, iris.target, folds=3, shuffle=True)
# for train, valid in folder:
#     gnb.fit(iris.data[train], iris.target.astype(np.int)[train])
#     y_pred = gnb.predict(iris.data[valid])
#     target = iris.target[valid]
#     print(f"Acc: {Accuracy()(y_pred, target)}")
#     print(f"Precision: {Precision()(y_pred, target)}")
#     print(f"Recall: {Recall()(y_pred, target)}")
#     print(f"F1: {F1()(y_pred, target)}")
#     # print(f"Confusion matrix: {ConfusionMatrix(normalize=True)(y_pred, target.astype(np.int))}")
#     print("Number of mislabeled points out of a total %d points : %d" % (
#     target.shape[0], (target != y_pred).sum()))
#
#     if isinstance(gnb, GaussianNaiveBayes):
#         break

    # folder = StratifiedKFold(iris.data[:102], iris.target[:102])
    # for train_index, test_index in folder:
    #     print(train_index)
