from src.experiments import steps
from src.data_loaders import WineLoader, PimaDiabetesLoader

import tqdm

dataset = PimaDiabetesLoader
pipeline = [
    steps.CreateDir(dataset),
    steps.BasisExperiment(None),
    steps.CrossValidationExperiment(dataset),
    steps.DiscretizationMethodsExperiment(dataset, range(1, 10)),
    steps.DataSmoothingExperiment(dataset),
    steps.GaussianPrioriExperiment(dataset),
    steps.FinalExperiment(None)
]


def run():
    aggregator = {}
    for step in tqdm.tqdm(pipeline):
        result = step.perform(aggregator)
        if result is not None:
            aggregator.update(result)


if __name__ == '__main__':
    run()
