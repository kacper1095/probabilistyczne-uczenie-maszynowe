import os
import datetime

PROJECT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))

DATA_DIR = os.path.join(PROJECT_DIR, 'data')

DATASETS_PATHS = {
    'diabetes':      os.path.join(DATA_DIR, 'diabetes'),
    'glass':         os.path.join(DATA_DIR, 'glass'),
    'iris':          os.path.join(DATA_DIR, 'iris'),
    'wine':          os.path.join(DATA_DIR, 'wine'),
    'pima-diabetes': os.path.join(DATA_DIR, 'pima-diabetes'),
    'abalone':       os.path.join(DATA_DIR, 'abalone')
}


EXPERIMENTS_DIR = os.path.join(PROJECT_DIR, 'experiments')

EPSILON = 1e-10


def get_time_stamp():
    return datetime.datetime.now().strftime('%HH_%MM_%dd_%mm_%Yy')
