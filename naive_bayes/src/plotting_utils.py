import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np


def create_pandas_data_frame(data, colnames):
    if isinstance(data, list) or isinstance(data, tuple):
        return pd.DataFrame({c: d for c, d in zip(colnames, data)})
    else:
        return pd.DataFrame({colnames: data})


class Plotter:
    def __init__(self, output_path, figsize=(8, 8), **kwargs):
        self.output_path = output_path
        self.kwargs = kwargs
        self.figsize = figsize

    def plot(self, data):
        raise NotImplementedError


class PlottingPipeline(Plotter):
    def __init__(self, plotters, **kwargs):
        self.plotters = plotters
        self.kwargs = kwargs

    def plot(self, data):
        for plotter, d in zip(self.plotters, data):
            plotter.plot(d)


class ConfusionMatrix(Plotter):
    def __init__(self, output_path, vmin=None, vmax=None, fmt='.2f', square=True, xticklabels='auto',
                 yticklabels='auto', cmap='Blues', **kwargs):
        super().__init__(output_path, kwargs)
        self.kwargs = kwargs
        self.vmin = vmin
        self.vmax = vmax
        self.fmt = fmt
        self.square = square
        self.xticklabels = xticklabels
        self.yticklabels = yticklabels
        self.cmap = cmap

    def plot(self, data):
        plt.figure(self.figsize)
        plt.ylabel('Prawdziwe klasy')
        plt.xlabel('Predykowane klasy')
        # data = data.astype('float') / data.sum(axis=1)[:, np.newaxis]
        ax = sns.heatmap(data, vmin=self.vmin, vmax=self.vmax, cmap=self.cmap,
                         square=self.square, annot=True, xticklabels=self.xticklabels,
                         yticklabels=self.yticklabels, fmt=self.fmt, cbar=False)
        ax.savefig(self.output_path)


class ParameterPlotter(Plotter):
    def __init__(self, output_path, x_title, y_title, title, plots_titles, **kwargs):
        super().__init__(output_path, **kwargs)
        self.x_title = x_title
        self.y_title = y_title
        self.title = title
        self.plots_titles = plots_titles

    def plot(self, data):
        params, results = data
        ax = None
        plt.figure(self.figsize)
        plt.title(self.title)
        plt.xlabel(self.x_title)
        plt.ylabel(self.y_title)
        for title, (train, val) in zip(self.plots_titles, results):
            ax = sns.factorplot(params, train, hue='kind', ax=ax, linestyles=["-", ":"])
        ax.savefig(self.output_path)


class BarParameterPlotter(Plotter):
    def __init__(self, output_path, x_title, y_title, title, **kwargs):
        super().__init__(output_path, **kwargs)
        self.x_title = x_title
        self.y_title = y_title
        self.title = title

    def plot(self, data):
        params, results = data
        ax = None
        plt.figure(self.figsize)
        plt.title(self.title)
        plt.xlabel(self.x_title)
        plt.ylabel(self.y_title)
        for param, res in zip(params, results):
            ax = sns.barplot(param, res, errwidth=0, errcolor='0', ax=ax)
        ax.savefig(self.output_path)
