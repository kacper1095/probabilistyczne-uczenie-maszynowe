from src.common import DATASETS_PATHS

import pandas as pd
import numpy as np
import os.path as osp


class DataLoader:
    def __init__(self, dataset_path, data_columns, class_column):
        self.data_path = dataset_path
        self.data_columns = data_columns
        self.class_column = class_column

        self._target = None
        self._data = None
        self._classes_names = []

        self._preprocess()

    @property
    def data(self):
        return self._data

    @property
    def target(self):
        return self._target

    @property
    def classes_names(self):
        return self._classes_names

    def _preprocess(self):
        raise NotImplementedError

    def _preprocess_data(self, data):
        raise NotImplementedError

    def _preprocess_target(self, target):
        raise NotImplementedError


class IrisLoader(DataLoader):

    def __init__(self):
        super().__init__(DATASETS_PATHS['iris'],
                         ["Sepal Length", "Sepal Width", "Petal Length", "Petal Width"],
                         ["Class"])

    def _preprocess(self):
        loaded_data = pd.read_csv(osp.join(self.data_path, 'iris.data'),
                                  names=self.data_columns + self.class_column)

        self._data = self._preprocess_data(loaded_data[self.data_columns])
        self._target = self._preprocess_target(loaded_data[self.class_column])

    def _preprocess_data(self, data):
        data = data.as_matrix()
        return data

    def _preprocess_target(self, target):
        classes = target.as_matrix()
        unique_classes = np.unique(classes)
        for i, cls in enumerate(unique_classes):
            classes[classes == cls] = i
            self._classes_names.append(cls)
        classes = classes.astype(np.int)
        return np.squeeze(classes)


class DiabetesLoader(DataLoader):
    def __init__(self):
        super().__init__(DATASETS_PATHS['diabetes'],
                         ["Date", "Time", "Code"],
                         ["Value"])

    def _preprocess(self):
        df = None
        for i in range(1, 71):
            num = str(i).zfill(2)
            csv_data = pd.read_csv(osp.join(self.data_path, 'Diabetes-Data', 'data-{}'.format(num)),
                                   names=self.data_columns + self.class_column,
                                   sep='\t')
            csv_data['DateTime'] = csv_data[['Date', 'Time']].apply(lambda x: ' '.join(x), axis=1)
            csv_data['DateTime'] = pd.to_datetime(csv_data['DateTime'])
            csv_data['DateTime'] = csv_data['DateTime'].astype('int')
            csv_data['DateTime'] = np.log(csv_data['DateTime'])
            csv_data['DateTime'] = (csv_data['DateTime'] - np.min(csv_data['DateTime'])) / (
                    csv_data['DateTime'].max() - csv_data['DateTime'].min())
            csv_data = csv_data.drop(['Date'], axis=1)
            csv_data = csv_data.drop(['Time'], axis=1)
            if df is None:
                df = csv_data
            else:
                df = df.append(csv_data)

        self.data_columns = ['DateTime'] + self.data_columns[2:]
        self._data = self._preprocess_data(df[self.data_columns])
        self._target = self._preprocess_target(df[self.class_column])

    def _preprocess_data(self, data):
        data = data.as_matrix()
        return data

    def _preprocess_target(self, target):
        classes = target.as_matrix()
        classes = classes.astype(np.int)
        return np.squeeze(classes)


class WineLoader(DataLoader):
    def __init__(self):
        super().__init__(DATASETS_PATHS['wine'],
                         ["Alcohol", "Malic acid", "Ash", "Alcalinity of ash",
                          "Magnesium", "Total phenols", "Flavanoids", "Nonflavanoid phenols",
                          "Proanthocyanins", "Color intensity", "Hue", "OD280/OD315 of diluted wines",
                          "Proline"],
                         ["Class"])

    def _preprocess(self):
        loaded_data = pd.read_csv(osp.join(self.data_path, 'wine.data'),
                                  names=self.class_column + self.data_columns)
        self._data = self._preprocess_data(loaded_data[self.data_columns])
        self._target = self._preprocess_target(loaded_data[self.class_column])

    def _preprocess_data(self, data):
        data = data.as_matrix()
        return data

    def _preprocess_target(self, target):
        classes = target.as_matrix()
        classes = classes.astype(np.int)
        self._classes_names += list(np.unique(classes))
        classes -= 1  # So each class range starts from 0

        return np.squeeze(classes)


class GlassLoader(DataLoader):
    def __init__(self):
        super().__init__(DATASETS_PATHS['glass'],
                         ["RI", "Na", "Mg", "Al", "Si", "K", "Ca", "Ba", "Fe"],
                         ["Type of glass"])

    def _preprocess(self):
        unnecessary_column = ["Id"]
        loaded_data = pd.read_csv(osp.join(self.data_path, 'glass.data'),
                                  names=unnecessary_column + self.data_columns + self.class_column)
        loaded_data = loaded_data.drop(["Id"], axis=1)

        self._data = self._preprocess_data(loaded_data[self.data_columns])
        self._target = self._preprocess_target(loaded_data[self.class_column])

    def _preprocess_data(self, data):
        data = data.as_matrix()
        return data

    def _preprocess_target(self, target):
        classes = target.as_matrix()
        classes = classes.astype(np.int)
        self._classes_names += list(np.unique(classes))
        classes -= 1  # So each class range starts from 0
        classes[classes > 3] -= 1  # Class 5 does not exist in the dataset
        return np.squeeze(classes)


class PimaDiabetesLoader(DataLoader):
    def __init__(self):
        super().__init__(
            DATASETS_PATHS['pima-diabetes'],
            ["Number of times pregnant",
             "Plasma glucose concentration a 2 hours in an oral glucose tolerance test",
             "Diastolic blood pressure (mm Hg)",
             "Triceps skin fold thickness (mm)",
             "2-Hour serum insulin (mu U/ml)",
             "Body mass index (weight in kg/(height in m)^2)",
             "Diabetes pedigree function",
             "Age (years)"],
            ["Class"]
        )

    def _preprocess(self):
        loaded_data = pd.read_csv(osp.join(self.data_path, 'pima-indians-diabetes.data'),
                                  names=self.data_columns + self.class_column)
        loaded_data = loaded_data.drop(self._get_unique_rows(loaded_data).index)
        self._data = self._preprocess_data(loaded_data[self.data_columns])
        self._target = self._preprocess_target(loaded_data[self.class_column])

    def _preprocess_data(self, data):
        data = data.as_matrix()
        return data

    def _preprocess_target(self, target):
        classes = target.as_matrix()
        classes = classes.astype(np.int)
        self._classes_names += list(np.unique(classes))
        return np.squeeze(classes)

    def _get_unique_rows(self, data):
        rows_with_zeros = [
            data[data["Triceps skin fold thickness (mm)"] == 0],
            data[data["Plasma glucose concentration a 2 hours in an oral glucose tolerance test"] == 0],
            data[data["Diastolic blood pressure (mm Hg)"] == 0],
            data[data["Body mass index (weight in kg/(height in m)^2)"] == 0],
            data[data["Diabetes pedigree function"] == 0],
            data[data["2-Hour serum insulin (mu U/ml)"] == 0]
        ]
        rows_with_zeros = pd.concat(rows_with_zeros)
        rows_with_zeros = rows_with_zeros.drop_duplicates()
        return rows_with_zeros


class AbaloneLoader(DataLoader):
    def __init__(self):
        super().__init__(
            DATASETS_PATHS['abalone'],
            [
                "Sex",
                "Length",
                "Diameter",
                "Height",
                "Whole weight",
                "Shucked weight",
                "Viscera weight",
                "Shell weight"
            ],
            ["Rings"]
        )

    def _preprocess(self):
        loaded_data = pd.read_csv(osp.join(self.data_path, 'abalone.data'),
                                  names=self.data_columns + self.class_column)
        self._data = self._preprocess_data(loaded_data[self.data_columns])
        self._target = self._preprocess_target(loaded_data[self.class_column])

    def _preprocess_data(self, data):
        data = data.replace({'Sex': {'M': 0, 'F': 1, 'I': 2}})
        data = data.as_matrix()
        return data

    def _preprocess_target(self, target):
        classes = target.as_matrix()
        classes = classes.astype(np.int) - 1
        self._classes_names += list(np.unique(classes))

        return np.squeeze(classes)


all_datasets_with_names = [
    ('iris', IrisLoader),
    ('wine', WineLoader),
    ('glass', GlassLoader),
    ('pima', PimaDiabetesLoader),
    ('abalone', AbaloneLoader)
]

if __name__ == '__main__':
    print(np.unique(DiabetesLoader().target))
