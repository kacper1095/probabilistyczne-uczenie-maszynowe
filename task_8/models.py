import warnings
from collections import Counter, OrderedDict
from nltk.corpus import brown
from nltk import FreqDist

import numpy as np


class Model:
    def __init__(self, normalize_params=True):
        self.alphas = np.zeros(())
        self.known_words = set()
        self.normalize_params = normalize_params

        self._word_mapping = OrderedDict()
        self._internal_counter = np.zeros(())
        self._index_to_word = OrderedDict()
        self._word_to_index = OrderedDict()
        self._total_terms = 0

        self.build()

    def build(self):
        self.alphas.fill(1)
        self.known_words.clear()

        self._total_terms = 0
        self._word_mapping.clear()
        self._index_to_word.clear()
        self._word_to_index.clear()
        self._internal_counter.fill(0)

    def fit(self, x):
        counter = Counter(x)
        brown_freq = FreqDist(i.lower() for i in brown.words())
        total_terms = len(counter.items()) + len(brown_freq.items())
        self.alphas = np.zeros((total_terms,))
        index_offset = 0
        for i, (word, freq) in enumerate(counter.items()):
            self.alphas[i] = freq
            self.known_words.add(word)
            self._word_mapping[word] = np.zeros((total_terms,))
            self._word_mapping[word][i] = 1
            self._index_to_word[i] = word
            index_offset += 1

        self._internal_counter = np.zeros((total_terms,))

        self.alphas = np.array(self.alphas)
        if self.normalize_params:
            self.alphas /= self.alphas.min()
        return self

    def fit_thetas(self, x):
        counter = Counter(x)
        brown_freq = FreqDist(i.lower() for i in brown.words())
        total_terms = len(counter.items()) + len(brown_freq.items())
        self._internal_counter = np.zeros((total_terms,))
        self._total_terms = total_terms
        index_offset = 0
        for i, (word, freq) in enumerate(counter.items()):
            self._internal_counter[i] = freq / total_terms
            self.known_words.add(word)
            self._word_mapping[word] = np.zeros((total_terms,))
            self._word_mapping[word][i] = 1
            self._word_to_index[word] = i
            self._index_to_word[i] = word
            index_offset += 1
        self.alphas = np.ones((total_terms,))
        return self

    def fit_params(self, x):
        counter = Counter(x)
        brown_freq = FreqDist(i.lower() for i in brown.words())
        total_terms = len(counter.items()) + len(brown_freq.items())
        self.alphas = np.zeros((self._total_terms,))
        for i, (word, freq) in enumerate(counter.items()):
            if word in counter.keys():
                continue
            self.alphas[self._word_mapping[word]] = freq
        if self.normalize_params:
            self.alphas /= self.alphas.min()
        return self

    def score(self, x):
        correct = 0
        total = len(x)
        for word in x:
            if word not in self.known_words:
                # warnings.warn(f'Unknown word: {word}')
                continue
            correct += self.map() == word
            self._internal_counter += self._word_mapping[word]
        return correct / total

    def score_not_online(self, x):
        correct = 0
        total = len(x)
        for word in x:
            if word not in self.known_words:
                # warnings.warn(f'Unknown word: {word}')
                continue
            correct += self.map() == word
        return correct / total

    def map(self):
        denominator = self._internal_counter.sum() + self.alphas.sum() - len(self.alphas)
        params = (self._internal_counter + self.alphas - 1) / denominator
        return self._index_to_word[np.argmax(params)]

    def predict(self, x):
        predicted = []
        for word in x:
            if word not in self.known_words:
                warnings.warn(f'Unknown word: {word}')
                continue
            predicted.append(self.map())
            self._internal_counter += self._word_mapping[word]
        return predicted
