from datasets import *
from models import *

model = Model(False)
dataset = Article(True)

train, test = dataset.train_test_split()

print(model.fit(train).score(test))
