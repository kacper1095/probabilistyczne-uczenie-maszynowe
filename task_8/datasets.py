import os
import os.path as osp

import requests
import spacy
import numpy as np
from nltk import word_tokenize, FreqDist
from nltk.corpus import brown

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
DATASETS_PATH = os.path.join(PROJECT_DIR, 'data')

URL = "https://gist.githubusercontent.com/sebleier/554280/raw/" \
      "7e0e4a1ce04c2bb7bd41089c9821dbcf6d0c786c/NLTK's%2520list%2520of%2520english%2520stopwords"
STOP_WORDS = [
    str(w) for w in requests.get(URL).content.decode('utf-8').split('\n')
]

DICT_SIZE = 3000
DATA_OFFSET = 50


class _Dataset:
    def __init__(self, text_file_path, use_lemmatizer):
        with open(text_file_path) as f:
            self.file_content = f.read()

        self.nlp = spacy.load('en')
        dictionary = FreqDist(i.lower() for i in brown.words())
        self.dictionary = dictionary.most_common(DICT_SIZE + DATA_OFFSET)[DATA_OFFSET:]
        self.dictionary, self.alphas = zip(*self.dictionary)

        if use_lemmatizer:
            self.content = [
                token.lemma_ for token in self.nlp(' '.join(self.tide_up_text(self.file_content)))
            ]
        else:
            self.content = self.tide_up_text(self.file_content)

    def train_test_split(self, train_percentage_size=0.75):
        boundary = int(train_percentage_size * len(self.content))
        train = self.content[:boundary]
        test = self.content[boundary:]
        return train, test

    def tide_up_text(self, text):
        text = word_tokenize(text)
        text = [w.lower() for w in text]
        for i, word in enumerate(text):
            for sign in "\"':;~!@#$%^&*()_+{}[]|\\?/>.<, -=":
                word = word.replace(sign, '')
            text[i] = word

        text = [w for w in text
                if w not in "\"':;~!@#$%^&*()_+{}[]|\\?/>.<, -=" and w not in STOP_WORDS and len(w) > 0]
        return text


def NothingElseMatters(use_lemmatizer):
    return _Dataset(osp.join(DATASETS_PATH, 'lyrics', 'nothing_else_matters.txt'),
                    use_lemmatizer)


def Hobbits(use_lemmatizer):
    return _Dataset(osp.join(DATASETS_PATH, 'lyrics', 'hobbits.txt'), use_lemmatizer)


def Interstellar(use_lemmatizer):
    return _Dataset(osp.join(DATASETS_PATH, 'poetry', 'interstellar.txt'), use_lemmatizer)


def RomeoAndJuliet(use_lemmatizer):
    return _Dataset(osp.join(DATASETS_PATH, 'poetry', 'romeo_juliet.txt'), use_lemmatizer)


def Article(use_lemmatizer):
    return _Dataset(osp.join(DATASETS_PATH, 'prose', 'article.txt'), use_lemmatizer)


def Lotr(use_lemmatizer):
    return _Dataset(osp.join(DATASETS_PATH, 'prose', 'lotr.txt'), use_lemmatizer)


__all__ = [
    "NothingElseMatters", "Hobbits", "Interstellar", "RomeoAndJuliet",
    "Article", "Lotr", "datasets_with_names"
]

datasets_with_names = [
    ('nothing else matters', NothingElseMatters),
    ('hobbits', Hobbits),
    ('interstellar', Interstellar),
    ('romeo and juliet', RomeoAndJuliet),
    ('article', Article),
    ('lotr', Lotr)
]

if __name__ == '__main__':
    print(Article(False).train_test_split()[0])
