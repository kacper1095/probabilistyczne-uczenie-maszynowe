import numpy as np
import math
from collections import OrderedDict

LIMIT = 500
EPSILON = 1e-20


def primes_sieve2(limit):
    a = [True] * limit  # Initialize the primality list
    a[0] = a[1] = False

    for (i, isprime) in enumerate(a):
        if isprime:
            yield i
            for n in range(i * i, limit, i):  # Mark factors non-prime
                a[n] = False


class Hypothesis:
    def __init__(self, data=None, lambda_fn=None, a_min=1, a_max=LIMIT):
        self.lambda_fn = lambda_fn
        self.data = data
        self.a_min = a_min
        self.a_max = a_max

        if lambda_fn is not None and data is None:
            self._generate_data()

    def __contains__(self, item):
        return item in self.data

    def __len__(self):
        return len(self.data)

    def all_data_in_hypothesis(self, data):
        return int(all(item in self for item in data))

    def data_from_to(self, a_min, a_max):
        return self.data[a_min:a_max]

    def data_to(self, a_max):
        return self.data[:a_max]

    def get_likelihood(self, item):
        return (self.all_data_in_hypothesis(item) * 1 / len(self)) ** len(item)

    def get_likelihood_with_prior(self, item, prior):
        return (self.all_data_in_hypothesis(item) * prior / len(self)) ** len(item)

    def get_likelihood_normalized_with_prior(self, item, prior, normalizing_factor):
        return self.get_likelihood_with_prior(item, prior) / normalizing_factor

    def get_log_likelihood(self, item):
        return self.get_likelihood(item)

    def get_log_aposteriori(self, item, prior, normalizing_factor):
        return self.get_likelihood_with_prior(item, prior)

    def _generate_data(self):
        self.data = []
        for i in range(self.a_min, self.a_max + 1):
            if self.lambda_fn(i):
                self.data.append(i)

    def __str__(self):
        return self.__class__.__name__


# hypotheses = {
#     'numbers ending with 9': Hypothesis(lambda_fn=lambda x: str(x).endswith('9')),
#     'even numbers': Hypothesis(lambda_fn=lambda x: x % 2 == 0),
#     'powers of 2 plus 37': Hypothesis(lambda_fn=lambda x: x in [2 ** i for i in range(0, 10)] or x == 37),
#     'powers of 2 except 32': Hypothesis(lambda_fn=lambda x: x in [2 ** i for i in range(0, 10)] and x != 32),
#     'powers of 2': Hypothesis(lambda_fn=lambda x: x in [2 ** i for i in range(0, 10)]),
#     'odd numbers': Hypothesis(lambda_fn=lambda x: x % 2 != 0),
#     'prime numbers': Hypothesis(lambda_fn=lambda x: x in [num for num in primes_sieve2(LIMIT)])
# }


hypotheses = [
    ('even', Hypothesis(lambda_fn=lambda x: x % 2 == 0)),
    ('odd', Hypothesis(lambda_fn=lambda x: x % 2 != 0)),
    ('squares', Hypothesis(lambda_fn=lambda x: np.sqrt(x) - int(np.sqrt(x)) < 1e-8)),
]

for i in range(3, 11):
    hypotheses.append((f'mult of {i}', Hypothesis(lambda_fn=lambda x: x % i == 0)))

for i in range(1, 10):
    hypotheses.append((f'ends in {i}', Hypothesis(lambda_fn=lambda x: str(x).endswith(str(i)))))

for i in range(2, 11):
    hypotheses.append((f'powers of {i}', Hypothesis(
        lambda_fn=lambda x: math.log(x, i) - int(math.log(x, i)) < 1e-8
    )))

hypotheses += [
    ('all', Hypothesis(lambda_fn=lambda x: True)),
    ('powers of 2 + {37}', Hypothesis(lambda_fn=lambda x: math.log(x, 2) - int(math.log(x, 2)) < 1e-8 or x == 37)),
    ('powers of 2 - {32}', Hypothesis(lambda_fn=lambda x: math.log(x, 2) - int(math.log(x, 2)) < 1e-8 and x != 32)),
]

hypotheses = OrderedDict(hypotheses)


def add_hypothesis(name, lambda_fn):
    global hypotheses
    hypotheses[name] = Hypothesis(lambda_fn=lambda_fn)


def get_hypotheses_names():
    return [name for name in hypotheses.keys()]
